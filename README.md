# Scripting, SSH, and SCP
## Bash scripting
Bash scripting is the ability to declarityly type bash commands that provision a machine on a file and then running it then running it against a machine

Profisioning a machine includes:
* making files and directories
* editing / configuring files
* installing software
* starting and stopping files
* creating init files
* sending files and code over to the computer

## SSH
SSH is very suseful to securely log into a computer at a distance
It allows us to open a terminal on said compyuter with shell.
We can then use all our bash knowledge to configure the machine.

Main command:
```bash
# remote login to a machine
# syntax ssh <option> <user>@machine.ip
# Example
ssh -i ~/.ssh/ch9_shared.pem ubuntu@63.33.42.103
ssh -i ~/.ssh/ch9_shared.pem thor@63.33.42.103
```
### SSH directly into user account
copy authorized_keys file into users .ssh file

### Direct commands
ssh -i ~/.ssh/ch9_shared.pem ubuntu@63.33.42.103 <command>
runs the given command on the computer of the ip, then comes back to your home computer

### SCP
Imagine it like 'mv' but with ssh key and remote computers

```bash
# Syntax
# scp -i  ~/.ssh/ch9_shared.pem <source/file> ubuntu@63.33.42.103  <target>
# scp -i  ~/.ssh/ch9_shared.pem <source/file> <user>@<ip>:<path/to/location>

scp -i  ~/.ssh/ch9_shared.pem bashscript_101.sh ubuntu@63.33.42.103:/users/ubuntu/james

# Syntax to send whole folder
scp -i  ~/.ssh/ch9_shared.pem -r james_website ubuntu@63.33.42.103:/users/ubuntu/james
```
## Packages and services
sudo apt update
updates the repository

sudo apt install <package>
installs the package

sudo service <package> start
start the software

sudo service <package> stops

sudo service <package> restart

sudo service <package> status

can use systemctl instead of service
##
##